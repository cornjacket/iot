package onboardproc

import (
	"fmt"

	"bitbucket.org/cornjacket/iot/message"
)

var onboardFunc func(int, message.UpPacket) bool
var onboardFuncIsSet bool = false

// NewWorker creates, and returns a new Worker object. Its only argument
// is a channel that the worker can add itself to whenever it is done its
// work.
func NewWorker(id int, workerQueue chan chan WorkRequest) Worker {
	// Create, and return the worker.
	worker := Worker{
		ID:          id,
		Work:        make(chan WorkRequest),
		WorkerQueue: workerQueue,
		QuitChan:    make(chan bool)}

	return worker
}

func registerOnboardHandler(fp func(int, message.UpPacket) bool) {
    onboardFunc = fp
    onboardFuncIsSet = true
    fmt.Printf("RegisterOnboardHandler invoked\n") 
}


type Worker struct {
	ID          int
	Work        chan WorkRequest
	WorkerQueue chan chan WorkRequest
	QuitChan    chan bool
}

// This function "starts" the worker by starting a goroutine, that is
// an infinite "for-select" loop.
func (w *Worker) Start() {
	go func() {
		for {
			// Add ourselves into the worker queue.
			w.WorkerQueue <- w.Work

			select {
			case work := <-w.Work:
				// Receive a work request.
				fmt.Printf("worker%d: Received onboard request %s, delaying for %f seconds\n", w.ID, work.Packet.Data, work.Delay.Seconds())

    			if onboardFuncIsSet == true {
    				fmt.Printf("worker: pre onboardFunc() invoked.\n")
    				onboardFunc(w.ID, work.Packet)
    			} else {
    			  fmt.Printf("worker%d: ERROR: onboardFunc is not set.\n", w.ID)
	
    			}

			case <-w.QuitChan:
				// We have been asked to stop.
				fmt.Printf("worker%d stopping\n", w.ID)
				return
			}
		}
	}()
}

// Stop tells the worker to stop listening for work requests.
//
// Note that the worker will only stop *after* it has finished its work.
func (w *Worker) Stop() {
	go func() {
		w.QuitChan <- true
	}()
}
