package onboardproc

import "time"
import "bitbucket.org/cornjacket/iot/message"

type WorkRequest struct {
	IsTest bool
	Name   string
	Delay  time.Duration
	Packet message.UpPacket
}
