package onboardproc

import (
	// Standard library packets
	"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
	"github.com/julienschmidt/httprouter"
)

// PacketController represents the controller for operating on the Packet resource
type (
	OnboardController struct {
	}
)

// this needs to change from message.UpPacket to message.OnboardReq
func NewOnboardController(r *httprouter.Router, NWorkers int, fp func(int, message.UpPacket) bool) *OnboardController {
	// Start the dispatcher.
	fmt.Println("Starting the dispatcher")
	
	StartDispatcher(NWorkers)

	oc := OnboardController{}
	// register onboard handler
	registerOnboardHandler(fp) // if i want the function to be attached to the controller instance then i need to attach it to the struct
	// Post an onboard resource
	r.POST("/onboard", oc.CreateOnboard)

	return &oc
}

// CreatePacket creates a new packet resource
// returns the original packet so that testing can confirm the packet was received - strictly speaking this is not necessary.
func (pc OnboardController) CreateOnboard(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	// Stub an example packet to be populated from the body
	p := message.UpPacket{}

	// Populate the packet data
	json.NewDecoder(r.Body).Decode(&p)

	// Marshal provided interface into JSON strucutre
	pj, _ := json.Marshal(p)

	// DRT = testing
	// Now, we take the delay, and the person's name, and make a WorkRequest out of them.
	work := WorkRequest{Name: "David", Delay: 3000000, Packet: p}

	// DRT - testing
	// Push the work onto the queue.
	WorkQueue <- work
	fmt.Println("Work request queued")

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", pj)
}
