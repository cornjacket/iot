package tallocrespproc 

import (
	"fmt"

	// Third party packages
	// this shouldn't be referenced from worker, only from controllers - not sure if I agree with this...
	//"github.com/cornjacket/iot/message"
)

var tAllocRespProcFunc func(int, TallocResp) bool
var tAllocRespProcFuncIsSet bool = false

// NewWorker creates, and returns a new Worker object. Its only argument
// is a channel that the worker can add itself to whenever it is done its
// work.
func NewWorker(id int, workerQueue chan chan WorkRequest) Worker {
	// Create, and return the worker.
	worker := Worker{
		ID:          id,
		Work:        make(chan WorkRequest),
		WorkerQueue: workerQueue,
		QuitChan:    make(chan bool)}

	return worker
}

func registerTAllocRespProcHandler(fp func(int, TallocResp) bool) {
    tAllocRespProcFunc = fp
    tAllocRespProcFuncIsSet = true
    fmt.Printf("SLIB\tWORKER\tINIT\tRegisterTAllocRespProcHandler invoked\n")
}


type Worker struct {
	ID          int
	Work        chan WorkRequest
	WorkerQueue chan chan WorkRequest
	QuitChan    chan bool
}

// This function "starts" the worker by starting a goroutine, that is
// an infinite "for-select" loop.
func (w *Worker) Start() {
	go func() {
		for {
			// Add ourselves into the worker queue.
			w.WorkerQueue <- w.Work

			select {
			case work := <-w.Work:
				// Receive a work request.
                                //fmt.Printf("%s\tSLIB\tPKTPROC\tWORKER\tINFO\tworker%d: Received packet request %s\n", serviceName, w.ID, work.Packet.Data)

				if tAllocRespProcFuncIsSet == true {
					//fmt.Printf("worker: pre pktFunc() invoked.\n")
					//fmt.Printf("%s\tSLIB\tPKTPROC\tWORKER\tDEBUG\tworker%d: Invoking pktFunc\n", serviceName, w.ID)
					tAllocRespProcFunc(w.ID, work.TallocResp)
				} else {
					//fmt.Printf("worker%d: ERROR: pktFunc is not set.\n", w.ID)
					fmt.Printf("SLIB\tWORKER\tERROR\tworker%d: tAllocRespProcFunc has not been set\n", w.ID)

				}

			case <-w.QuitChan:
				// We have been asked to stop.
				//fmt.Printf("worker%d stopping\n", w.ID)
                                fmt.Printf("SLIB\tWORKER\tINFO\tworker%d: stopping.\n", w.ID)
				return
			}
		}
	}()
}

// Stop tells the worker to stop listening for work requests.
//
// Note that the worker will only stop *after* it has finished its work.
func (w *Worker) Stop() {
	go func() {
		w.QuitChan <- true
	}()
}
