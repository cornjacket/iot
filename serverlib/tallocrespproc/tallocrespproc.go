package tallocrespproc

import (
	// Standard library packets
	"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	"github.com/julienschmidt/httprouter"
)

type TallocResp struct {
	//GatewayEui	string // not sure if i want/need this here
	NodeEui		string
	Tslot		int
	Code		int // not sure how i will use this. maybe indicate whether valid or oversubscribed - 0 will be valid, otherwise error code
	Test		bool
	Ts		uint64
}


var verbose = false

func NewResponseHandlerFunc(NWorkers int, fp func(int, TallocResp) bool) func(http.ResponseWriter, *http.Request, httprouter.Params) {

	// Start the dispatcher.
	fmt.Printf("TallocResp\tStarting the dispatcher at %s\n")

	StartDispatcher(NWorkers)

	// register handler
	registerTAllocRespProcHandler(fp)

	return responseHandler 
}

func responseHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	// Stub an example packet to be populated from the body
	tar := TallocResp{}

	// Populate the packet data
	json.NewDecoder(r.Body).Decode(&tar)

	// Marshal provided interface into JSON strucutre
	tj, _ := json.Marshal(tar)

	work := WorkRequest{TallocResp: tar}

	// Push the work onto the queue.
	WorkQueue <- work
	//fmt.Println("Work request queued")

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", tj)
}
