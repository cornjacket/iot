package tallocrespproc

import "fmt"

var WorkerQueue chan chan WorkRequest

func StartDispatcher(nworkers int) {
	// First, initialize the channel we are going to but the workers' work channels into.
	WorkerQueue = make(chan chan WorkRequest, nworkers)

	// Now, create all of our workers.
	for i := 0; i < nworkers; i++ {
		fmt.Printf("SLIB\ttallocrespproc\tDISPATCH\tINIT\tStarting worker %d\n", i+1)
		worker := NewWorker(i+1, WorkerQueue)
		worker.Start()
	}

	go func() {
		for {
			select {
			case work := <-WorkQueue:
		                //fmt.Printf("%s\tSLIB\tPKTPROC\tDISPATCH\tINFO\tReceived work request\n", serviceName)
				go func() {
					worker := <-WorkerQueue

					//fmt.Println("Dispatching work request")
					if verbose {
						fmt.Printf("SLIB\ttallocrespproc\tWork request received and dispatched: %v\n", work)
					}
					worker <- work
				}()
			}
		}
	}()
}
