package lastboot 

import (
	// Standard library packets
	"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
	"github.com/julienschmidt/httprouter"
)

// VersionController represents the controller for operating on the Version resource. Read only resource.
type (
	LastbootController struct{}
)

var serviceName = "UNKNOWN_SERVICE"
var serviceLastboot message.LastBoot

func NewController(sName string, r *httprouter.Router, bootTime int64) *LastbootController {
	serviceName = sName
	//serviceLastboot = lastboot
	serviceLastboot = message.LastBoot{Version: 0, LastBoot: bootTime}
	lbc := &LastbootController{}
	fmt.Printf("%s\tSLIB\tLASTBOOT\tINIT GET /lastboot invoked at %d.\n", serviceName, bootTime)
	r.GET("/lora_iot/lastboot", lbc.Get) // All services will support this interface
	return lbc
}

// Get retrieves an individual Version resource
func (lbc LastbootController) Get(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	// Marshal provided interface into JSON strucutre
	lj, _ := json.Marshal(serviceLastboot)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Printf("%s\tSLIB\tLASTBOOT\tINFO\tGET /lastboot invoked: %v\n", serviceName, serviceLastboot)
	fmt.Fprintf(w, "%s", lj)

}

// TODO: I need to handle the slib and clib api version numbers...
