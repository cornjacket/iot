package notifyproc

import "bitbucket.org/cornjacket/iot/message"

type WorkRequest struct {
	IsTest bool
	Notification message.Notification
}
