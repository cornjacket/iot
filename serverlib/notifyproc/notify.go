package notifyproc

import (
	// Standard library packets
	"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
	//"github.com/julienschmidt/httprouter"
)

type (
	NotifyController struct {
	}
)

// this needs to change from message.UpPacket to message.OnboardReq
//func NewNotifyController(r *httprouter.Router, NWorkers int, fp func(int, message.Notification) bool) *NotifyController {
func NewNotifyController(NWorkers int, fp func(int, message.Notification) bool) *NotifyController {
	// Start the dispatcher.
	fmt.Println("Starting the dispatcher")

	StartDispatcher(NWorkers)

	nc := NotifyController{}
	// register packet handler
	registerNotifyHandler(fp) // if i want the function to be attached to the controller instance then i need to attach it to the struct
	// Post a packet resource
	//r.POST("/notify", nc.CreateNotify)
	http.HandleFunc("/notify", nc.CreateNotify)

	return &nc
}

// returns the original packet so that testing can confirm the packet was received - strictly speaking this is not necessary.
//func (nc NotifyController) CreateNotify(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
func (nc NotifyController) CreateNotify(w http.ResponseWriter, r *http.Request) {

	// Stub an example packet to be populated from the body
	n := message.Notification{}

	// Populate the packet data
	json.NewDecoder(r.Body).Decode(&n)

	// Marshal provided interface into JSON strucutre
	nj, _ := json.Marshal(n)

	// DRT = testing
	// Now, we take the delay, and the person's name, and make a WorkRequest out of them.
	work := WorkRequest{Notification: n}

	// DRT - testing
	// Push the work onto the queue.
	WorkQueue <- work
	fmt.Println("Work request queued")

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", nj)
}
