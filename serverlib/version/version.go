package version

import (
	// Standard library packets
	"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
	"github.com/julienschmidt/httprouter"
)

// VersionController represents the controller for operating on the Version resource. Read only resource.
type (
	VersionController struct{}
)

var serviceVersion message.Version

func NewController(r *httprouter.Router, version message.Version) *VersionController {
	serviceVersion = version
	vc := &VersionController{}
	r.GET("/version", vc.Get) // All services will support this interface
	return vc
}

// Get retrieves an individual Version resource
func (vc VersionController) Get(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	// Marshal provided interface into JSON strucutre
	vrj, _ := json.Marshal(serviceVersion)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Printf("version: GET /version invoked. %s\n", vrj)
	fmt.Fprintf(w, "%s", vrj)

}

// TODO: I need to handle the slib and clib api version numbers...
