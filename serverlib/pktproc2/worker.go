package pktproc2

import (
	"fmt"

	// Third party packages
	// this shouldn't be referenced from worker, only from controllers - not sure if I agree with this...
	"bitbucket.org/cornjacket/iot/message"
)

var pktFunc func(int, message.UpPacket) bool
var pktFuncIsSet bool = false

// NewWorker creates, and returns a new Worker object. Its only argument
// is a channel that the worker can add itself to whenever it is done its
// work.
func NewWorker(id int, workerQueue chan chan WorkRequest) Worker {
	// Create, and return the worker.
	worker := Worker{
		ID:          id,
		Work:        make(chan WorkRequest),
		WorkerQueue: workerQueue,
		QuitChan:    make(chan bool)}

	return worker
}

func registerPacketHandler(fp func(int, message.UpPacket) bool) {
    pktFunc = fp
    pktFuncIsSet = true
    fmt.Printf("SLIB\tPKTPROC\tWORKER\tINIT\tRegisterPacketHandler invoked\n")
}


type Worker struct {
	ID          int
	Work        chan WorkRequest
	WorkerQueue chan chan WorkRequest
	QuitChan    chan bool
}

// This function "starts" the worker by starting a goroutine, that is
// an infinite "for-select" loop.
func (w *Worker) Start() {
	go func() {
		for {
			// Add ourselves into the worker queue.
			w.WorkerQueue <- w.Work

			select {
			case work := <-w.Work:
				// Receive a work request.
                                //fmt.Printf("SLIB\tPKTPROC\tWORKER\tINFO\tworker%d: Received packet request %s\n", w.ID, work.Packet.Data)

				if pktFuncIsSet == true {
					//fmt.Printf("worker: pre pktFunc() invoked.\n")
					//fmt.Printf("SLIB\tPKTPROC\tWORKER\tDEBUG\tworker%d: Invoking pktFunc\n", w.ID)
					pktFunc(w.ID, work.Packet)
				} else {
					//fmt.Printf("worker%d: ERROR: pktFunc is not set.\n", w.ID)
					fmt.Printf("SLIB\tPKTPROC\tWORKER\tERROR\tworker%d: pktFunc has not been set\n", w.ID)

				}

			case <-w.QuitChan:
				// We have been asked to stop.
				//fmt.Printf("worker%d stopping\n", w.ID)
                                fmt.Printf("SLIB\tPKTPROC\tWORKER\tINFO\tworker%d: stopping.\n", w.ID)
				return
			}
		}
	}()
}

// Stop tells the worker to stop listening for work requests.
//
// Note that the worker will only stop *after* it has finished its work.
func (w *Worker) Stop() {
	go func() {
		w.QuitChan <- true
	}()
}
