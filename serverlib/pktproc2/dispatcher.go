package pktproc2

import "fmt"

var WorkerQueue chan chan WorkRequest

func StartDispatcher(nworkers int) {
	// First, initialize the channel we are going to but the workers' work channels into.
	WorkerQueue = make(chan chan WorkRequest, nworkers)

	// Now, create all of our workers.
	for i := 0; i < nworkers; i++ {
		fmt.Printf("SLIB\tPKTPROC\tDISPATCH\tINIT\tStarting worker %d\n", i+1)
		worker := NewWorker(i+1, WorkerQueue)
		worker.Start()
	}

	go func() {
		for {
			select {
			case work := <-WorkQueue:
		                //fmt.Printf("SLIB\tPKTPROC\tDISPATCH\tINFO\tReceived work request\n")
				go func() {
					worker := <-WorkerQueue

					//fmt.Println("Dispatching work request")
					if verbose {
						fmt.Printf("SLIB\tPKTPROC\tWork request received and dispatched: %v\n", work)
					}
					worker <- work
				}()
			}
		}
	}()
}
