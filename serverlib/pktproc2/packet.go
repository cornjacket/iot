package pktproc2

import (
	// Standard library packets
	"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
	"github.com/julienschmidt/httprouter"
)

var verbose = false

func NewPacketHandlerFunc( NWorkers int, fp func(int, message.UpPacket) bool) func(http.ResponseWriter, *http.Request, httprouter.Params) {

	// Start the dispatcher.
	fmt.Printf("SLIB\tPKTPROC\tINFO\tStarting the dispatcher.\n")

	StartDispatcher(NWorkers)

	// register packet handler
	registerPacketHandler(fp) // if i want the function to be attached to the controller instance then i need to attach it to the struct

	return packetHandler 
}


// returns the original packet so that testing can confirm the packet was received - this is not really necessary.
func packetHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	// Stub an example packet to be populated from the body
	p := message.UpPacket{}

	// Populate the packet data
	json.NewDecoder(r.Body).Decode(&p)

	// Marshal provided interface into JSON strucutre
	pj, _ := json.Marshal(p)

	work := WorkRequest{Packet: p}

	// Push the work onto the queue.
	WorkQueue <- work
	//fmt.Println("Work request queued")
	//fmt.Printf("SLIB\tPKTPROC\tCREATE\tINFO\tWork Request Queued: %v\n", work)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", pj)
}

