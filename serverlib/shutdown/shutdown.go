package shutdown

import (
	// Standard library packets
	"encoding/json"
	"net/http"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
	"github.com/julienschmidt/httprouter"
)

// https://stackoverflow.com/questions/40498371/how-to-send-an-interrupt-signal-in-golang

// ShutdownController represents the controller for operating on the Shutdown resource. Read only resource.
type (
	ShutdownController struct{}
)

var sName = "UNKNOWN"
var serviceShutdown message.Shutdown

var stopChannel chan os.Signal

func NewController(r *httprouter.Router, shutdown message.Shutdown, serviceName string) *ShutdownController {
	sName = serviceName
	stopChannel = make(chan os.Signal)
	signal.Notify(stopChannel, syscall.SIGINT)
	signal.Notify(stopChannel, syscall.SIGTERM)
    go func() {
        sig := <-stopChannel
        fmt.Printf("%s\tSLIB\tSTATUS\tSHUTDOWN\tCaught sig: %+v. Waiting 2 sec before exit.\n", sName, sig)
        time.Sleep(2*time.Second)
        fmt.Printf("%s\tSLIB\tSTATUS\tSHUTDOWN\tExit.\n", sName)
        os.Exit(0)
    }()

	serviceShutdown = shutdown
	sc := &ShutdownController{}
	r.POST("/shutdown", sc.Shutdown) // All services will support this interface 
	return sc
}

// returns the original packet so that testing can confirm the packet was received - strictly speaking this is not necessary.
func (sc ShutdownController) Shutdown(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	// TODO: should we confirm the service number matches the service's service number prior to taking the service down. Should there be a magic number that is used when putting a service up
	// that must be sent to the service in order for the shutdown command to function?

	// Stub an example packet to be populated from the body
	s := message.Shutdown{}

	// Populate the packet data
	json.NewDecoder(r.Body).Decode(&s)

	// TODO: should we confirm the service number matches the service's service number prior to taking the service down. Should there be a magic number that is used when putting a service up
	// that must be sent to the service in order for the shutdown command to function?

	// Marshal provided interface into JSON strucutre
	sj, _ := json.Marshal(s)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Printf("%s\tSLIB\tSTATUS\tSHUTDOWN\tPost /shutdown received: %s\n", sName, sj)
	fmt.Fprintf(w, "%s", sj)

	if s.Service == serviceShutdown.Service {
	        fmt.Printf("%s\tSLIB\tSTATUS\tSHUTDOWN\tserviceID match", sName)
		stopChannel <- syscall.SIGINT
	} else {
	        fmt.Printf("%s\tSLIB\tSTATUS\tSHUTDOWN\tserviceID mismatch: %d, %d\n", sName, s.Service, serviceShutdown.Service)
	}
}
