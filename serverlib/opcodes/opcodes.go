package opcodes

import (
	// Standard library packets
	"encoding/json"
	"net/http"
	"fmt"

	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
	"github.com/julienschmidt/httprouter"
)

// OpcodesController represents the controller for operating on the Opcodes resource. Read only resource called by OpRouter Service
type (
	OpcodesController struct{}
)

var opcodesArray [3]int
var serviceID message.ServiceId

func NewController(r *httprouter.Router, id message.ServiceId, opArray [3]int) *OpcodesController {
	serviceID = id
	opcodesArray = opArray
	oc := &OpcodesController{}
	r.GET("/opcodes", oc.GetOpcodes) // All terminal services will support this interface 
	return oc
}

// GetOpcodes retrieves an individual Opcodes resource
func (oc OpcodesController) GetOpcodes(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	//or := models.Opcodes{ OpcodeList: [3]int{20, 30, -1}, // -1 added so not zero padded. <--- these should be defined in the specific app.go file, i.e. soil_sensor.go, ...
	or := message.Opcodes{ OpcodeList: opcodesArray,
                           ServiceID: serviceID, // <---- defined in the specific app.go file, i.e. primary_valve.go
                          } // This specific service supports these opcodes - other services will support other opcodes

	//or := models.GetOpcodes()

	// Marshal provided interface into JSON strucutre
	orj, _ := json.Marshal(or)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Printf("ServiceID: %d, GET /opcodes invoked. %s\n", serviceID, orj)
	fmt.Fprintf(w, "%s", orj)

}
