package cardinality

import (
	// Standard library packets
	"encoding/json"
	"net/http"
	"fmt"

	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
	"github.com/julienschmidt/httprouter"
)

// OpcodesController represents the controller for operating on the Opcodes resource. Read only resource called by OpRouter Service
type (
	CardinalityController struct{}
)

var networkCardinality int

func NewController(r *httprouter.Router, cardinality int) *CardinalityController {
	if cardinality < 0 {
		cardinality = 1
	}
	if cardinality > 8 {
		cardinality = 8
	}
	networkCardinality = cardinality
	cc := &CardinalityController{}
	r.GET("/cardinality", cc.GetCardinality) // All terminal services will support this interface 
	return cc
}

// GetOpcodes retrieves an individual Opcodes resource
func (cc CardinalityController) GetCardinality(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	cr := message.NetworkCardinality{ Cardinality: networkCardinality } 

	// Marshal provided interface into JSON strucutre
	crj, _ := json.Marshal(cr)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Printf("GET /cardinality invoked. %s\n", crj)
	fmt.Fprintf(w, "%s", crj)

}
