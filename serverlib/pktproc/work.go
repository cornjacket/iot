package pktproc

import "bitbucket.org/cornjacket/iot/message"

type WorkRequest struct {
	IsTest bool
	Packet message.UpPacket
}
