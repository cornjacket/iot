package pktproc

import (
	// Standard library packets
	"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
	"github.com/julienschmidt/httprouter"
)

// PacketController represents the controller for operating on the Packet resource
type (
	PacketController struct {
	}
)

var serviceName = "UNKNOWN"

func NewPacketController(name string, r *httprouter.Router, NWorkers int, fp func(int, message.UpPacket) bool) *PacketController {

	serviceName = name
	// Start the dispatcher.
	fmt.Printf("%s\tSLIB\tPKTPROC\tNEW\tINFO\tStarting the dispatcher\n", serviceName)

	StartDispatcher(NWorkers)

	pc := PacketController{}
	// register packet handler
	registerPacketHandler(fp) // if i want the function to be attached to the controller instance then i need to attach it to the struct
	// Post a packet resource
	r.POST("/packet", pc.CreatePacket)

	return &pc
}

// CreatePacket creates a new packet resource
// returns the original packet so that testing can confirm the packet was received - strictly speaking this is not necessary.
func (pc PacketController) CreatePacket(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	// Stub an example packet to be populated from the body
	p := message.UpPacket{}

	// Populate the packet data
	json.NewDecoder(r.Body).Decode(&p)

	// Marshal provided interface into JSON strucutre
	pj, _ := json.Marshal(p)

	// DRT = testing
	// Now, we take the delay, and the person's name, and make a WorkRequest out of them.
	work := WorkRequest{Packet: p}

	// DRT - testing
	// Push the work onto the queue.
	WorkQueue <- work
	//fmt.Println("Work request queued")
	fmt.Printf("%s\tSLIB\tPKTPROC\tCREATE\tINFO\tWork Request Queued: %v\n", work)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", pj)
}

