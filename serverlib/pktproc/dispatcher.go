package pktproc

import "fmt"

var WorkerQueue chan chan WorkRequest

func StartDispatcher(nworkers int) {
	// First, initialize the channel we are going to but the workers' work channels into.
	WorkerQueue = make(chan chan WorkRequest, nworkers)

	// Now, create all of our workers.
	for i := 0; i < nworkers; i++ {
		//fmt.Printf("%s\tSLIB\tPKTPROC\tDISPATCH\tINIT\tStarting worker %d\n", serviceName, i+1)
		fmt.Printf("%s\tSLIB\tPKTPROC\tDISPATCH\tINIT\tStarting worker %d\t", serviceName, i+1)
		worker := NewWorker(i+1, WorkerQueue)
		worker.Start()
	}
	fmt.Println()

	go func() {
		for {
			select {
			case work := <-WorkQueue:
				//fmt.Println("Received work requeust")
		                fmt.Printf("%s\tSLIB\tPKTPROC\tDISPATCH\tINFO\tReceived work request\n", serviceName)
				go func() {
					worker := <-WorkerQueue

					//fmt.Println("Dispatching work request")
		                        fmt.Printf("%s\tSLIB\tPKTPROC\tDISPATCH\tINFO\tReceived work request\n", serviceName)
					worker <- work
				}()
			}
		}
	}()
}
