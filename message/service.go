package message

type ServiceRegister struct {
	Key                int       `json:"key"` // temporary mechanism to insure that received packet is valid
	Id                 ServiceId `json:"id"`
	PortNum            string    `json:"portnum"`
	AppNum             AppId     `json:"appnum"`
	OpcodesSupport     bool      `json:"opcodessupport"`
	OnboardSupport     bool      `json:"onboardsupport"`
	NotifySupport      bool      `json:"notifysupport"`
	PacketSupport      bool      `json:"packetsupport"`
	CardinalitySupport bool      `json:"cardinalitysupport"`

	// just added, need to add support in the Service Registration
	Version        int `json:"version"`        // ServiceRegister version, this indicates what fields are included in this message - under lib control
	ServiceVersion int `json:"serviceversion"` // version number of the service - under app control
	SlibApiLevel   int `json:"slibapilevel"`   // server lib api level
	ClibApiLevel   int `json:"clibapilevel"`   // client lib api level
	BootTime       int64 `json:"boottime"`
	Name	       string `json:"name"`
	DataValue      string `json:"datavalue"`
	DestId             []ServiceId `json:"destid"`
	DestUrl            []string `json:"desturl"`
	DestStatus         []DestServiceStatus `json:"deststatus"`

}

// This should be changed to ServiceEntry
type ServiceResp struct {
	Valid              bool `json:"valid"`
	Id                 ServiceId `json:"_id"`
	IpAddress          string `json:"ipaddress"`
	PortNum            string `json:"portnum"`
	AppNum             AppId `json:"appnum"`
	OpcodesSupport     bool `json:"opcodessupport"`
	OnboardSupport     bool `json:"onboarsupport"`
	NotifySupport      bool `json:"notifysupport"`
	PacketSupport      bool `json:"packetsupport"`
	CardinalitySupport bool `json:"cardinalitysupport"`
	// just added, need to add support in the Service Registration
	Version            int `json:"version"`        // ServiceRegister version, this indicates what fields are included in this message - under lib control
	ServiceVersion     int `json:"serviceversion"` // version number of the service - under app control
	SlibApiLevel       int `json:"slibapilevel"`   // server lib api level
	ClibApiLevel       int `json:"clibapilevel"`   // client lib api level
	LastBoot           LastBoot `json:"lastboot"`
	Name	           string `json:"name"`
	DataValue          string `json:"datavalue"`
	DestId             []ServiceId `json:"destid"`
	DestUrl            []string `json:"desturl"`
	DestStatus         []DestServiceStatus `json:"deststatus"`

}


type ServiceId int

const (
	ReservedServiceId       ServiceId = 0 // reserved for test app purposes only
	DiscoveryServiceId	ServiceId = 1 // used as a placeholder for shutdown identification - how does this work? - moved to be in range of service monitor
	OnboardServiceId        ServiceId = 2 // formerly cloudsync
	DownTrafficPolicerId    ServiceId = 3
	SystemExceptionId       ServiceId = 4
	NetworkMonitorId        ServiceId = 5
	LastUpdateId            ServiceId = 6
	GatewayMonitorId        ServiceId = 7
	AirTrafficControlId     ServiceId = 8
	NodeErrorId             ServiceId = 9
	NodeStatusId            ServiceId = 10
	CalendarCompId          ServiceId = 11
	EnvironmentMonitorId    ServiceId = 12
	IngestionNodeId         ServiceId = 13 // for redundant ingestion nodes
	DualValveControlId      ServiceId = 14
	DualArrayControlId      ServiceId = 15
	SentekSensorId          ServiceId = 16
	AquaCheckSensorId       ServiceId = 17
	PrimaryValveSchedulerId ServiceId = 18
	DualArraySchedulerId    ServiceId = 19
	FlowIntegratorSensorId  ServiceId = 20
	PressureSensorId        ServiceId = 21
	DecryptionId            ServiceId = 22
	FaultRecoveryId         ServiceId = 23 // Handles Long Term Storage and Recovery
	PrimaryValveControlId   ServiceId = 24
	TapDefaultOprouter      ServiceId = 25 // not sure if I agree with the type id concept. need to think how I want the system.
	MicroserviceManagerId   ServiceId = 26 // determines any loops in system, via ttl within packet - is this really a separte service - Should it be NetworkManager? Should it just be a tool, either server or client
	PrimaryValveMonitorId   ServiceId = 27 // is this the same as PrimaryValveControl ?? 
	DualValveMonitorId	ServiceId = 28 // is this the same as PrimaryValveControl ?? 
	NecromonId              ServiceId = 29 // should this be discovery to keep with the portNum = (3000 + serviceId) convention
	LegacyIngestionId       ServiceId = 30
	LoadBalancerId          ServiceId = 31  // why does the loadbalancer have such a high number?
	OpcodeRouterId          ServiceId = 32 // this was initially 255 but I am decreasing to lower number. Should that matter?
	VirtualBotNetId		ServiceId = 33
	LoraOtaControlOrchestrationId ServiceId = 34
	DualArrayErrorMonitorId       ServiceId = 35
	NetworkTestAppId        ServiceId = 36 // test for now - maybe eventually turn into network app
	PktForwarderId		ServiceId = 37 // adding just to get code to compile. service id's will be deprecated... - this could be CmdHndlr
	ViewHndlrId		ServiceId = 38 // adding just to get code to compile. service id's will be deprecated... 
)

var MaxServiceID ServiceId = 255 //previously 32, brought down. Does this have a consequence? 
var MinServiceID ServiceId = 0

type DestServiceStatus int

const (
	StatusFail              DestServiceStatus = 0
	StatusOk                DestServiceStatus = 1
	StatusUnknown           DestServiceStatus = 2 // not sure how this would be used...
)

// Not sure if we need to use AppId since the node may be in an unconfigured state. But at present the node is pre-configured with an AppId
type AppId int

// How to create a constant that can be used as an array dimension?

const (
	UnconfiguredAppId AppId = 0
	TestAppId         AppId = 999
	NullId            AppId = 1000
)

// This can go much higher for the node
var MaxAppNum AppId = 999 // 1000 is not valid
var MinAppNum AppId = 0
