package message

// provide a generic mechanism for passing a long a message. Either in int via the type field or by using a name/value string pair
type Notification struct {
	Version int  `json:"version"`
	Ts   uint64  `json:"ts"` // is this necessary
	Eui  string  `json:"EUI"`
	Name string  `json:"name"`
	Value string  `json:"value"`
	Type int     `json:"type"` // This could be an enumerated type to make it easier.
}
