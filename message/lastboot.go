package message

type LastBoot struct {
	Version		int	// if not set, then implies version 0
	LastBoot	int64
}
