package message


type GatewayDescriptor struct {
	Rssi	int     `json:"rssi"`
	Snr	float32 `json:"snr"`
	Ts	uint64  `json:"ts"`
	GwEui	string  `json:"gweui"`
	Lat	float32 `json:"lat"`
	Lon	float32 `json:"lon"`
}

// This definition should work for both the gw packet as well as the rx packet
type UpPacket struct {
	Dr   string  `json:"dr"`
	Ts   uint64  `json:"ts"`
	Eui  string  `json:"EUI"`
	Ack  bool    `json:"ack"`
	Cmd  string  `json:"cmd"`
	Snr  float32 `json:"snr"`
	Data string  `json:"data"`
	Fcnt int     `json:"fcnt"`
	Freq uint64  `json:"freq"`
	Port int     `json:"port"`
	Rssi int     `json:"rssi"`
	Gws  []GatewayDescriptor `json:"gws"`
}
