package message

type Shutdown struct {
	Name		string		`jaon:"name"`
	Service		ServiceId	`json:"service"`
	MagicNumber	int		`json:"magicnumber"`
}
