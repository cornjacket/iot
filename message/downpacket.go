package message

// Downstream packet sent to Downstream Traffic Policer
type DownPacket struct {
	Eui  string  `json:"EUI"`
	Data string  `json:"data"`
	Ageout bool  `json:"ageout"`
	Timeout int  `json:"timeout"`
}

// Downstream packet sent to Network Server
type NetworkServerDownPacket struct {
	Appid string  `json:"appid"`
	Cmd string  `json:"cmd"`
	Eui  string  `json:"EUI"`
	Port int  `json:"port"`
	Confirmed bool  `json:"confirmed"`
	Data string  `json:"data"`
}

