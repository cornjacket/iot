package message

// The Opcode structure is used to provide an application's opcodes to the OpRouter service. Also included in the Opcode structure is the service id
// so that the opRouter can verify is the correct service ID.

// QUESTION: Changing this to []byte changes the result of what is returned back from an array of 3 bytes to what looks like a pointer
// But I would like a variable length array so I can re-use this Opcodes struct definition in other places, so how do I do that?
type Opcodes struct {
	OpcodeList [3]int `json:"opcodes"`
	ServiceID  ServiceId `json:"serviceid"`
}