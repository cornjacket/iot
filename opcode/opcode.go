package opcode

type Opcode byte

const (
   DeferredDualArray          Opcode = 0xEF
   LegacyDualArray            Opcode = 0xFA 
)

func Get(oparr []byte) Opcode {
   return Opcode(oparr[0])
}
