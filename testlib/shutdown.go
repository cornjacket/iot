package test

import (
	// Standard library packets
	"net/http"
	"fmt"
	"os"
	"io"
	//"io/ioutil"
	"encoding/json"
	"bytes"
	//"strconv"
	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
)

func ShutdownService(path string, serviceID message.ServiceId, magicNumber int) {

    if path != "" {	
	   msg := message.Shutdown{Service: serviceID, MagicNumber: magicNumber}

	   b := new(bytes.Buffer)
	   json.NewEncoder(b).Encode(msg)

        full_path := path+"/shutdown"
        fmt.Printf("test.ShutdownService: invoked on %s!\n", full_path)
    	res, _ := http.Post(full_path, "application/json; charset=utf-8", b)
    	// todo: do an err check here
        io.Copy(os.Stdout, res.Body)
    
	   // probably need a check here to see if it succeeded. DRT
    } else {
        fmt.Println("test.ShutdownService: path is empty!")
    }
}
