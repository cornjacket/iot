package test

import (
	// Standard library packets
	"net/http"
	"fmt"
	"os"
	"io"
	//"io/ioutil"
	"encoding/json"
	"bytes"
	//"strconv"
	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
)

// should url include the portNum or be separate
func SendPacket(path string, upPacket message.UpPacket) {

    if path != "" {	
	   b := new(bytes.Buffer)
	   json.NewEncoder(b).Encode(upPacket)

        full_path := path+"/packet"
        fmt.Printf("test.SendPacket: invoked on %s!\n", full_path)
    	res, _ := http.Post(full_path, "application/json; charset=utf-8", b)
    	// todo: do an err check here
        io.Copy(os.Stdout, res.Body)
    
	   // probably need a check here to see if it succeeded. DRT
    } else {
    	fmt.Println("test.SendPacket: path is empty!")
    }
}
