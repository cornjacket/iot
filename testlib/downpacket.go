package test

import (
	// Standard library packets
	"net/http"
	"fmt"
	"os"
	"io"
	//"io/ioutil"
	"encoding/json"
	"bytes"
	//"strconv"
	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
)

// should url include the portNum or be separate
func SendDownPacket(path string, downPacket message.DownPacket) {

    if path != "" {	
	   b := new(bytes.Buffer)
	   json.NewEncoder(b).Encode(downPacket)

        full_path := path+"/downstream"
        fmt.Printf("test.SendDownPacket: invoked on %s!\n", full_path)
    	res, _ := http.Post(full_path, "application/json; charset=utf-8", b)
    	// todo: do an err check here
        io.Copy(os.Stdout, res.Body)
    
	   // probably need a check here to see if it succeeded. DRT
    } else {
    	fmt.Println("test.SendDownPacket: path is empty!")
    }
}
