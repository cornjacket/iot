package calendarcomp

import (
	"bitbucket.org/cornjacket/iot/message"
	"bitbucket.org/cornjacket/iot/clientlib/notify"
)

func Notify(n message.Notification) {
	
	notify.Send(message.CalendarCompId, n) // this return true/false success/fail

}

// TODO: this function needs to reply back with err in case of failure and not terminate the program
func Init(discoveryEndpoint string) {

	notify.Init(discoveryEndpoint)

}
