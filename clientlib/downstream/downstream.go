package downstream

// Purpose: downstream allows services to downstream commands via the Downstream Traffic Policer.
// downstream is a client lib that requests from Discovery the location of the Downstream Traffic Policer.

import (
	// Standard library packets
	"net/http"
	"fmt"
	"os"
	"io"
	"encoding/json"
	"bytes"
	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
	"bitbucket.org/cornjacket/iot/clientlib/service"
)

var dtpURL string = "" // URL for Downstream Traffic Policer used to forward downstream packets to the node
var serviceName string = "UNKNOWN"

func Send(packet message.DownPacket) {

	if dtpURL != "" {
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(packet)

	fmt.Printf("%s\tCLIB\tDOWNSTREAM\tINFO\tSending packet to EUI: %s via DTP: %s.\n", serviceName, packet.Eui, dtpURL)
		res, _ := http.Post(dtpURL, "application/json; charset=utf-8", b)
		// todo: do an err check here
		io.Copy(os.Stdout, res.Body)

		// probably need a check here to see if it succeeded. DRT
	} else {
		fmt.Printf("%s\tCLIB\tDOWNSTREAM\tERROR\tDownstream URL not set. Not forwarding packet!!\n", serviceName)
	}
}

// TODO: this function needs to reply back with err in case of failure and not terminate the program
func Init(sName string, discoveryEndpoint string) bool {

    if sName == "" {
       fmt.Printf("%s\tCLIB\tDOWNSTREAM\tINIT\tERROR\tsName not initialized!\n", serviceName)
       return false
    }
    serviceName = sName
    if discoveryEndpoint == "" {
       fmt.Printf("%s\tCLIB\tDOWNSTREAM\tINIT\tERROR\tDiscovery endpoint not initialized!\n", serviceName)
       return false
    }
    var dtpID message.ServiceId = message.DownTrafficPolicerId
    sr := *service.GetService(discoveryEndpoint, dtpID)
    // what happens if unable to find dtp service, then how should that be handled?
    if sr.Valid == false {
	fmt.Printf("%s\tCLIB\tDOWNSTREAM\tERROR\tDownstream not found at Discovery!!!\n", serviceName)
	return false
    } else {
	dtpURL = "http://" + sr.IpAddress + ":" + sr.PortNum + "/downstream"
	fmt.Printf("%s\tCLIB\tDOWNSTREAM\tINFO\tDownstream Endpoint discovered at %s\n", serviceName, dtpURL)
	return true
    }

}
