package notify

import (
	// Standard library packets
	"net/http"
	"fmt"
	"os"
	"io"
	"encoding/json"
	"bytes"
	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
    	"bitbucket.org/cornjacket/iot/clientlib/service"
)

var notifyURL  [256]string
var urlIsValid [256]bool
var discoveryEndpoint string

func Init(discoveryURL string) {
	discoveryEndpoint = discoveryURL
    for i := 0; i <= 255; i++ {
        notifyURL[i] = ""
        urlIsValid[i] = false
    }
}


func Send(serviceId message.ServiceId, notification message.Notification) bool {
	
	if (!urlIsValid[int(serviceId)]) {
		get(serviceId)
	}
	if (urlIsValid[int(serviceId)]) {

		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(notification)

		fmt.Printf("notify.Send: sending notification to serviceId: %s\n", serviceId)
		res, _ := http.Post(notifyURL[int(serviceId)], "application/json; charset=utf-8", b)
		// todo: do an err check here
		io.Copy(os.Stdout, res.Body)
    
		// need a check here to see if it succeeded. DRT and if not return false
	}
	return true
}


func get(serviceId message.ServiceId) {

	if discoveryEndpoint != "" {
    	sr := *service.GetService(discoveryEndpoint, serviceId)
    	if sr.NotifySupport {
    		notifyURL[serviceId] = "http://" + sr.IpAddress + ":" + sr.PortNum + "/notify"
    		urlIsValid[serviceId] = true
    		fmt.Printf("notify.Get: serviceId: %d, url: %s\n", serviceId, notifyURL[serviceId])    	
    	}
    }

}
