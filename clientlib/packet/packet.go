package packet

// Purpose: packet allows services to forward packets.
// downstream is a client lib that requests from Discovery the location of the Downstream Traffic Policer.

import (
	// Standard library packets
	"net/http"
	"fmt"
	"os"
	"io"
	//"io/ioutil"
	"encoding/json"
	"bytes"
	//"strconv"
	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
    	"bitbucket.org/cornjacket/iot/clientlib/service"
)


// Send will use these serviceId to forward. Each entiry in packetURL array should also have bool isValid
var packetURL  [256]string // URL for Downstream Traffic Policer used to forward downstream packets to the node
var urlIsValid [256]bool
var discoveryEndpoint string

func Init(discoveryURL string) {
	discoveryEndpoint = discoveryURL
    for i := 0; i <= 255; i++ {
        packetURL[i] = ""
        urlIsValid[i] = false
    }
}


func Send(serviceId message.ServiceId, packet message.UpPacket) bool {
	
	fmt.Printf("packet.Send entrance \n")
	if (!urlIsValid[int(serviceId)]) {
		fmt.Printf("packet.Send 1\n")
		get(serviceId)
	}
	if (urlIsValid[int(serviceId)]) {

		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(packet)

		fmt.Printf("packet.Send: sending packet to serviceId: %s\n", serviceId)
		fmt.Printf("packet.Send 2\n")
		res, _ := http.Post(packetURL[int(serviceId)], "application/json; charset=utf-8", b)
		fmt.Printf("packet.Send 3\n")
		// todo: do an err check here
		io.Copy(os.Stdout, res.Body)
    
		// need a check here to see if it succeeded. DRT and if not return false
	}
	return true
}


func get(serviceId message.ServiceId) {

	if discoveryEndpoint != "" {
    	fmt.Printf("packet.get entrance\n")
    	sr := *service.GetService(discoveryEndpoint, serviceId)
    	fmt.Printf("packet.get 1")
    	if sr.PacketSupport {
    		fmt.Printf("packet.get 2\n")
    		packetURL[serviceId] = "http://" + sr.IpAddress + ":" + sr.PortNum + "/packet"
    		urlIsValid[serviceId] = true
    		fmt.Printf("packet: serviceId: %d, url: %s\n", serviceId, packetURL[serviceId])    	
    	}
    }

}

func print() { 
	fmt.Printf("testing pushing a new commit with a pushed tag 0.1.0\n")
}
