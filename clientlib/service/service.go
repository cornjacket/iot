package service

// Purpose: Service is a client that interfces with Discovery and registers a service or requests info on a service

// When registering a service, the service should indicate whether or not the service is a packet processing service (opcodes: true) and thus supports the
// GET /opcodes route. This will inform other services (i.e. OpRouter ) to add the service to its route table.

// TODO: There needs to be a way for services to be informed of new services added to discovery, or should that always happen.

import (
	// Standard library packets
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
)

var serviceName = "NOT_REGISTERED"

// should have a check in other modules that this has been called.
//func Init(name string) {
//	serviceName = name
//}

// TODO: Service name needs to be added to the Register parameter list....

// Should the discovery_endpoint be saved within the module so it does not need to be passed inside GetService.
// However we need to have a solution for whether the Discovery service is down and have a way to handle a secondary service.
func Register(discoveryEndpoint string, portNum string, serviceID message.ServiceId, appNum message.AppId, opcodeSupport bool, onboardSupport bool, notifySupport bool, packetSupport bool, cardinalitySupport bool,
	      bootTime int64, dataValue string, destId []message.ServiceId, destUrl []string, destStatus []message.DestServiceStatus) {

	// TODO: sanity check that len(destId) == len(destUrl) == len(destStatus) to make sure registration is valid

	if discoveryEndpoint != "" {
		msg := message.ServiceRegister{Key: 1234, Id: serviceID, PortNum: portNum, AppNum: appNum, OpcodesSupport: opcodeSupport,
			OnboardSupport: onboardSupport, NotifySupport: notifySupport, PacketSupport: packetSupport, CardinalitySupport: cardinalitySupport,
			BootTime: bootTime, Name: serviceName, DataValue: dataValue, DestId: destId, DestUrl: destUrl, DestStatus: destStatus}

		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(msg)

		fmt.Printf("SERVICE\tREGISTER\tservice: sending service id: %d and url to %s!\n", serviceID, discoveryEndpoint+"/service")
		res, _ := http.Post(discoveryEndpoint+"/service", "application/json; charset=utf-8", b)
		// todo: do an err check here
		io.Copy(os.Stdout, res.Body)

		// need a check here to see if it succeeded. DRT
	}
}

// TODO: this function needs to return a structure back to the application including pass/fail and the url for the service - is this true?

// TODO: this function needs to reply back with err in case of failure and not terminate the program
// NOTE: returning type message.ServiceResp did not work, I believe sr is of type *messageServiceResp. Now I am returning &sr. Research this better....
func GetService(discoveryEndpoint string, serviceID message.ServiceId) *message.ServiceResp {

	// TODO: I need to unmarshall contents into a struct that can be used by opRouter - remove this comment
	sr := new(message.ServiceResp)

	if discoveryEndpoint != "" {
		// Need to get the downstream virtual port address
		url := discoveryEndpoint + "/service/" + strconv.Itoa(int(serviceID))
		fmt.Printf("SERVICE\tGETSERVICE\tINFO\turl: %s\n", url)
		response, err := http.Get(url)
		if err != nil {
			fmt.Printf("SERVICE\tGETSERVICE\tERROR\t%s\n", err)
			os.Exit(1)
		} else {
			defer response.Body.Close()
			// should i check the status code first to see if it succeeded.
			contents, err := ioutil.ReadAll(response.Body)

			if err != nil {
				fmt.Printf("SERVICE\tGETSERVICE\tERROR\t%s\n", err)
				os.Exit(1)
			}
			fmt.Printf("SERVICE\tGETSERVICE\tINFO\tcontents: %s\n", string(contents))

			err = json.Unmarshal(contents, &sr)
			// TODO: need to check err
			if err != nil {
				fmt.Printf("SERVICE\tGETSERVICE\tERROR\t%s\n", err)
				os.Exit(1)
			}
			//fmt.Println("DRT ---- output after unmarshall")
			//fmt.Println(sr)
		}
	}
	return sr // need to have an err field that is returned
}
